#ifndef PROTOCOL_DEF_H__
#define PROTOCOL_DEF_H__

#define PKT_LOGIN			0
#define PKT_LOGOUT			1
//#define PKT_PUBLIC_MSG		2
#define PKT_PRIVATE_MSG		3
#define PKT_LOGIN_REPLY		4
#define PKT_ORDER           2
#define PKT_ORDER_REPLY     6
#define PKT_LOGIN_FAILD     5
typedef struct PktHeader
{
	int nPktType;
}PktHeader;

typedef struct LoginPkt
{
	PktHeader header;
	char szName[20];
	char szPassword[20];
}LoginPkt;

typedef struct LoginReplyPkt
{
	PktHeader header;
	int		  nRet;
}LoginReplyPkt;

typedef struct LogoutPkt
{
	PktHeader header;
	char szName[20];
}LogoutPkt;

typedef struct PublicMsgPkt
{
	PktHeader header;
	char szFormName[20];
	char szMsg[500];
}PublicMsgPkt;

typedef struct PrivateMsgPkt
{
	PktHeader header;
	char szFormName[20];
	char szToName[20];
	char szMsg[500];
}PrivateMsgPkt;

#endif //PROTOCOL_DEF_H__


