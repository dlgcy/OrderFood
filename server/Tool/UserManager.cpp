#include <stdio.h>
#include "UserManager.h"

PwdItem g_PwdItemList[1024]; //用户名、密码存储；
int g_nPwdItemNum;

AuthUser g_UserList[1024];  //在线用户；
int  g_nUserNum;

bool LoadPwdItems(const char* pszFileName)
{
	FILE* fp = fopen(pszFileName, "r");
	if(fp == NULL)
		return false;
	while(!feof(fp))
	{
		PwdItem pwdItem;
		fscanf(fp, "%s%s", pwdItem.szName, pwdItem.szPassword);
		g_PwdItemList[g_nPwdItemNum++] = pwdItem;
	}

	return true;
}

bool FindUser(const char* pszName, const char* pszPassword)
{
	for(int i=0; i<g_nPwdItemNum; i++)
	{
		if(strcmp(pszName, g_PwdItemList[i].szName) == 0)
		{
			return true;
		}
	}
	return false;
}

void AddUser(const char* pszName, SOCKET s)
{
	strcpy(g_UserList[g_nUserNum].szName, pszName);
	g_UserList[g_nUserNum].sock = s;
	++g_nUserNum;
}

void DeleteUser(const char* pszName)
{
	for(int i=0; i<g_nUserNum; i++)
	{
		if(strcmp(pszName, g_UserList[i].szName) == 0)
		{
			--g_nUserNum;
			while(i < g_nUserNum)
			{
				strcpy(g_UserList[i].szName, g_UserList[i+1].szName);
				g_UserList[i].sock = g_UserList[i+1].sock;
				++i;
			}
			return;
		}
	}	
}

bool HasAuth(SOCKET s)
{
	for(int i=0; i<g_nUserNum; i++)
	{
		if(g_UserList[i].sock == s)
			return true;
	}
	return false;
}
