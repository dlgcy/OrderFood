﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrderFood_WPF
{
    /// <summary>
    /// UC_FoodBtn.xaml 的交互逻辑
    /// </summary>
    public partial class UC_FoodBtn : UserControl
    {
        public FoodControl fc;
        Food food;
        public UC_FoodBtn()
        {
            InitializeComponent();
        }
        public UC_FoodBtn(FoodControl fc)
        {
            InitializeComponent();
            this.fc = fc;
            this.food = fc.food;
            btn.Content = food.food_name;
        }
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            fc.cb.IsChecked = false;
        }
    }
}
